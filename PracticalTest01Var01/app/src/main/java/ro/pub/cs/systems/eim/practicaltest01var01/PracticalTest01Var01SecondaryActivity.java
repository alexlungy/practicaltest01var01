package ro.pub.cs.systems.eim.practicaltest01var01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class PracticalTest01Var01SecondaryActivity extends AppCompatActivity {

    TextView txtReceived;
    Button btnRegister;
    Button btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practical_test01_var01_secondary);

        txtReceived = findViewById(R.id.txt_received);
        btnRegister = findViewById(R.id.btn_register);
        btnCancel = findViewById(R.id.btn_cancel);

        Intent intent = getIntent();
        if (intent != null) {
            String received = intent.getStringExtra(PracticalTest01Var01MainActivity.SENT_TEXT_KEY);
            txtReceived.setText(received);
        }

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(PracticalTest01Var01MainActivity.RESULT_REGISTER);
                finish();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(PracticalTest01Var01MainActivity.RESULT_CANCEL);
                finish();
            }
        });
    }
}
