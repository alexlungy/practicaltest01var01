package ro.pub.cs.systems.eim.practicaltest01var01;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;

import java.util.Calendar;
import java.util.Date;

public class PracticalTest01Var01Service extends Service {

    public static final String BROADCAST_ACTION = "EIM.broadcast";
    public static final String MESSAGE_SENT_KEY = "MESSAGE";
    public static final String DATE_KEY = "DATE";

    private String receivedText;

    public PracticalTest01Var01Service() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            receivedText = intent.getStringExtra(PracticalTest01Var01MainActivity.SENT_TEXT_KEY);

            SystemClock.sleep(5000);
            Date currentTime = Calendar.getInstance().getTime();

            Intent newIntent = new Intent();
            newIntent.setAction(BROADCAST_ACTION);
            newIntent.putExtra(MESSAGE_SENT_KEY, receivedText);
            newIntent.putExtra(DATE_KEY, currentTime.toString());
            sendBroadcast(newIntent);

        }

        return START_STICKY;
    }
}
