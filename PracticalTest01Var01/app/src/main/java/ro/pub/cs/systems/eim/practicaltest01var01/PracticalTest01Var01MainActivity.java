package ro.pub.cs.systems.eim.practicaltest01var01;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class PracticalTest01Var01MainActivity extends AppCompatActivity {


    private final String POINTS_SELECTED_KEY = "points_selected";

    public static final String TAG = "EIM";
    public static final String SENT_TEXT_KEY = "sent_text";
    public static final int REQUEST_CODE = 850;
    public static final int RESULT_REGISTER = 851;
    public static final int RESULT_CANCEL = 852;


    private final String NORTH = "North, ";
    private final String WEST = "West, ";
    private final String EAST = "East, ";
    private final String SOUTH = "South, ";


    private Button btnNorth;
    private Button btnWest;
    private Button btnEast;
    private Button btnSouth;
    private Button btnNavigate;
    private TextView txtInstructions;

    private int pointsSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practical_test01_var01_main);

        btnNorth = findViewById(R.id.btn_north);
        btnWest = findViewById(R.id.btn_west);
        btnEast = findViewById(R.id.btn_east);
        btnSouth = findViewById(R.id.btn_south);
        btnNavigate = findViewById(R.id.btn_navigate);
        txtInstructions = findViewById(R.id.txt_instructions);

        btnNorth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtInstructions.append(NORTH);
                IncrementPointsAndCheckService();
            }
        });

        btnWest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtInstructions.append(WEST);
                IncrementPointsAndCheckService();
            }
        });

        btnEast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtInstructions.append(EAST);
                IncrementPointsAndCheckService();
            }
        });

        btnSouth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtInstructions.append(SOUTH);
                IncrementPointsAndCheckService();
            }
        });

        btnNavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), PracticalTest01Var01SecondaryActivity.class);
                intent.putExtra(SENT_TEXT_KEY, txtInstructions.getText().toString());
                startActivityForResult(intent, REQUEST_CODE);
                txtInstructions.setText("");
                pointsSelected = 0;
            }
        });
    }

    private void IncrementPointsAndCheckService() {
        pointsSelected++;
        Log.d(TAG, pointsSelected + " points");
        if (pointsSelected >= 4) {
            Intent intent = new Intent(this, PracticalTest01Var01Service.class);
            intent.putExtra(SENT_TEXT_KEY, txtInstructions.getText().toString());
            startService(intent);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(POINTS_SELECTED_KEY, pointsSelected);
        Log.d(TAG, "Saving points selected; value: " + pointsSelected + " points");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        pointsSelected = savedInstanceState.getInt(POINTS_SELECTED_KEY);
        Log.d(TAG, "Restored points selected; value: " + pointsSelected + " points");
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        switch(requestCode) {
            case REQUEST_CODE:
                if (resultCode == RESULT_REGISTER) {
                    Toast.makeText(getApplicationContext(), "REGISTER",
                            Toast.LENGTH_SHORT).show();
                }
                if (resultCode == RESULT_CANCEL) {
                    Toast.makeText(getApplicationContext(), "CANCEL",
                            Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    public static class MyReceiver extends BroadcastReceiver {

        public MyReceiver() {
            super();
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String receivedText = intent.getStringExtra(PracticalTest01Var01Service.MESSAGE_SENT_KEY);
                String receivedDate = intent.getStringExtra(PracticalTest01Var01Service.DATE_KEY);
                Log.d(PracticalTest01Var01MainActivity.TAG, "TEXT: " + receivedText);
                Log.d(PracticalTest01Var01MainActivity.TAG, "DATE: " + receivedDate);
            }
        }
    }
}
